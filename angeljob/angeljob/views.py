from django.views.generic import TemplateView
from angeljob.utils import jobs, job

class HomeView(TemplateView):
    template_name='home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['jobs'] = jobs()
        return context

class JobView(TemplateView):
    template_name='job.html'

    def get_context_data(self, **kwargs):
        context = super(JobView, self).get_context_data(**kwargs)
        context['job'] = job(self.kwargs['jobid'])
        return context
