from django.conf.urls import patterns, include, url

from django.contrib import admin
from angeljob import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(
        regex=r'^$', 
        view=views.HomeView.as_view(), 
        name='home'
    ),
    url(
    	regex=r'^job/(?P<jobid>\d+)/$',
    	view=views.JobView.as_view(),
    	name='job'
    ),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
