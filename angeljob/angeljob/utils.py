import urllib2
import json
from pprint import pprint

def jobs():
	response = urllib2.urlopen("https://api.angel.co/1/jobs")

	text = response.read()

	fobj = open("data.json", 'w')
	fobj.write(text)
	fobj.close()

	json_data = open('data.json')

	data = json.load(json_data)
	jobs=[]

	for job in range(len(data["jobs"])):
		jobs.append(data["jobs"][job])

	return jobs

def job(jobid):
	url = "https://api.angel.co/1/jobs/{id}".format(id=jobid)
	response = urllib2.urlopen(url)

	text = response.read()

	fobj = open("job.json", 'w')
	fobj.write(text)
	fobj.close()

	json_data = open('job.json')

	data = json.load(json_data)
	return data
